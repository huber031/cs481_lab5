class Jobs {

  final String jTitle;
  final String jDescription;
  final String jImage;

  Jobs({this.jTitle, this.jDescription, this.jImage});

  static List<Jobs> allJobs()
  {
    var jobsList = new List<Jobs>();

    jobsList.add(new Jobs(jTitle: "Getaway Driver", jDescription: "Get the car and drive away", jImage: "getawaycar.png"));
    jobsList.add(new Jobs(jTitle: "Loan Shark", jDescription: "Ask people nicely for money", jImage: "loanshark.png"));
    jobsList.add(new Jobs(jTitle: "Soldier", jDescription: "Do all the dirty work", jImage: "soldier.png"));
    jobsList.add(new Jobs(jTitle: "Caporegime", jDescription: "Captain", jImage: "caporegime.png"));
    jobsList.add(new Jobs(jTitle: "Underboss", jDescription: "Second in command", jImage: "underboss.png"));
    jobsList.add(new Jobs(jTitle: "Capo", jDescription: "The Boss", jImage: "capo.png"));

    return jobsList;
  }
}