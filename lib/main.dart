import 'package:flutter/material.dart';
import 'Jobs.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: FormPage(),
    );
  }
}

//global strings that hold the variables from the form
String name;
String lname;
String age;
String famiglia;
String gender;


class FormPage extends StatefulWidget {
  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  //controllers that handle getting the input from the textfields
  final nameController = TextEditingController();
  final lastnController = TextEditingController();
  final ageController = TextEditingController();
  final famigliaController = TextEditingController();

  //holds initial value for dropdown as male
  String dropdownValue = 'Male';
  //global key for the form where it checks its state
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  //void function to use validated for the formKey which
  //runs the validator functions in the form to see if the all the validator functions check out
  void validated(){
    if(formKey.currentState.validate()){
      //Navigator pushes the page replacement to the appTabs which gets rid of the form if the form is valid
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => appTabs()));
    }
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    nameController.dispose();
    lastnController.dispose();
    ageController.dispose();
    famigliaController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black54,
        title: Text('Form'),
      ),
      body: ListView(
        children: [
          Form(
            //sets the key for the form to formKey
              key: formKey,
              child: Column(children: <Widget> [
                TextFormField(
                    //sets controller to nameController which gets the textfield input
                    controller: nameController,
                    //decoration used to show a hint for what to put
                    decoration: InputDecoration(hintText: 'First Name'),
                    //validator checks the input for the textfield and sees if it's empty
                    //if it is empty, when the user presses submit the validator msg shows
                    validator: (String value) {if(value.isEmpty)
                    {
                      return "Invalid: Cannot be empty.";
                    }else
                      return null;
                    }
                ),
                TextFormField(
                    controller: lastnController,
                    decoration: InputDecoration(hintText: 'Last Name'),
                    validator: (String value) {if(value.isEmpty)
                    {
                      return "Invalid: Cannot be empty.";
                    }else
                      return null;
                    }
                ),
                TextFormField(
                    controller: ageController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(hintText: 'Age'),
                    validator: (String value) {if(value.isEmpty)
                    {
                      return "Invalid: Cannot be empty.";
                    }else
                      return null;
                    }
                ),
                DropdownButtonFormField<String>(
                  //hint for dropdownbutton for form field so show what the button is for
                  decoration: (
                      InputDecoration(labelText: 'Gender')
                  ),
                  //sets the initial value to the dropdownValue declared earlier
                  value: dropdownValue,
                  //when it's changed, gets the new value and sets that as the dropdownValue
                  onChanged: (String newValue) {
                    setState(() {
                      dropdownValue = newValue;
                    });
                  },
                  //items for the dropdown menu that are in a list of type String
                  items: <String>['Male', 'Female', 'Other']
                  //maps the string list to the dropdownmenu choices
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      //when selected - set value to value and the displayed text to the new value
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
                TextFormField(
                    controller: famigliaController,
                    decoration: InputDecoration(hintText: 'Affiliated Famiglia'),
                    validator: (String value) {if(value.isEmpty)
                    {
                      return "Invalid: Cannot be empty.";
                    }else
                      return null;
                    }
                ),
                RaisedButton(
                  child: Text('Submit'),
                  onPressed: () {
                    //when the button is pressed, calls the validated function to check the form's validity
                    //then sets the global string variables to what was put in the form
                    validated();
                    name = nameController.text;
                    lname = lastnController.text;
                    age = ageController.text;
                    famiglia = famigliaController.text;
                    gender = dropdownValue;
                  },
                ),
              ])
          ),
        ]
      )
    );
  }
}


class appTabs extends StatefulWidget {
  @override
  _appTabsState createState() => _appTabsState();
}

class _appTabsState extends State<appTabs> {

  @override
  Widget build(BuildContext context) {
    //a list of Widgets that hold the pages for the tabs
    final tabPages = <Widget> [
      JobsTab(),
      ProfileTab(),
      FamigliaTab(),
    ];
    //a list of Tabs that holds the tabs and sets up their icons and tooltips
    final theTabs = <Tab>[
      const Tab(text: 'JOBS', icon: Tooltip(message: 'Jobs Tab', child: Icon(Icons.work, size: 34.0))),
      const Tab(text: 'YOU', icon: Tooltip(message: 'Profile Tab', child: Icon(Icons.account_circle, size: 34.0))),
      const Tab(text: 'FAMIGLIA', icon: Tooltip(message: 'Profile Tab', child: Icon(Icons.group, size: 34.0))),
    ];

    return DefaultTabController(
      //sets the length of the tabs  to the length of the list of tabs which is 2
      length: theTabs.length,
      child: Scaffold (
        appBar: AppBar(
          title: const Text('Mafia Job Finder'),
          backgroundColor: Colors.black54,
          //sets the tab bar
          bottom: TabBar(
            //the color that shows you what tab you're on
            indicatorColor: Colors.white54,
            //where you put the tabs that you build
            tabs: theTabs,
          ),
        ),
            //shows the body of the app in a tabbarview which shows the tabs and their pages
            body: TabBarView(
              //the pages are in order of the list of tabPages that was made earlier
          children: tabPages,
      )
      ),
    );
  }
}

//the page for the JobTab
class JobsTab extends StatelessWidget {

  //the list that holds all the jobs from the Job class that we set up in Jobs.dart
  final List<Jobs> _allJobs = Jobs.allJobs();
  //bool that holds the choice
  bool choice;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        //sets the itemcount to how many is in the jobs list
        itemCount: _allJobs.length,
        itemBuilder: _getJob,
      )
    );
  }


  Widget _getJob(BuildContext context, int index) {
    return new Card(
        child: new Column(
            children: <Widget> [
              new ListTile(
                  leading: Container(
                    //border for the images
                    decoration: BoxDecoration(
                    border: Border.all(
                    width: 2,
                    )
                    ),
                    child: Image.asset(
                      //sets the image for that card to be the image for whatever index we're in for the jobs list
                    "images/" + _allJobs[index].jImage,
                    fit: BoxFit.cover,
                    ),
                    ),
                  title: new Text(
                    //sets title to job title of whatever index we're in
                    _allJobs[index].jTitle,
                    style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                  ),
                subtitle: new Column(
                  //the rest of the content of the listTile
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget> [
                    Text("\n",
                    style: TextStyle(fontSize: 2.0) ),
                    //sets subtitle to job info of whatever index we're in
                    Text(_allJobs[index].jDescription),
                    //adds a row of Raised Buttons
                    Row(
                      children: [
                        Container(
                          //container used to set the button's height and width
                            padding: EdgeInsets.all(7.0),
                            height: 30,
                            width: 100,
                            child: RaisedButton(
                                    child: Text('Accept'),
                                    onPressed: () {
                                      //sets the choice == true
                                      //display the snackbar
                                    choice = true;
                                    showSnackBar(context);
                                }
                            )
                        ),
                        Container(
                            padding: EdgeInsets.all(7.0),
                            height: 30,
                            width: 100,
                            child: RaisedButton(
                                child: Text('Decline'),
                                onPressed: () {
                                  //sets the choice == false
                                  //display the snackbar
                                  choice = false;
                                  showSnackBar(context);
                                }
                            )
                        )
                      ],
                    )
                  ]
                )
              )
            ]
        )

    );
  }

  void showSnackBar(BuildContext context) {
    //the function to show the snackBar
    SnackBar theSnackBar = new SnackBar(
      //if choice is true, snack bar == accepted, if choice is false, snackbar == decline
      content: choice ? Text('Job accepted') : Text('Job declined'),
      action: SnackBarAction(
        label: 'Hide',
        onPressed: () {
        },
      ),
      duration: const Duration(seconds: 3),
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
    );
    //Snackbar always paired to the scaffold
    Scaffold.of(context).showSnackBar(theSnackBar);
  }

}

//the page for the ProfileTab
class ProfileTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          //set the color of the card
          color: Colors.black38,
          child: Column(
              children: [
                ListTile(
                  //leading is the picture
                  leading: profilePicture,
                  //title is the name given in the form
                  title: profileName,
                  //subtitle is the info provided by the form
                  subtitle: profileInfo,
                ),
                //Button bar to hold the buttons
                ButtonBar(
                  alignment: MainAxisAlignment.end,
                  children: [
                    //button to send emails, includes tooltip
                    IconButton(
                      icon: Icon(Icons.mail, color: Colors.white60),
                      tooltip: "Send Email",
                      onPressed: (){
                        showSnackBar(context);
                      },
                    ),
                    //button to share info, includes tooltip
                    IconButton(
                      icon: Icon(Icons.share, color: Colors.white60),
                      tooltip: "Share Info",
                      onPressed: (){
                        showSnackBar(context);
                      },
                    ),
                  ],
                )
              ]
          )
      ),);
  }
  void showSnackBar(BuildContext context) {
    //the function to show the snackBar
    SnackBar theSnackBar = new SnackBar(
      //if choice is true, snack bar == accepted, if choice is false, snackbar == decline
      content: Text("That button doesn't actually do anything, silly"),
      action: SnackBarAction(
        label: 'Then go away!',
        onPressed: () {
        },
      ),
      duration: const Duration(seconds: 3),
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
    );
    //Snackbar always paired to the scaffold
    Scaffold.of(context).showSnackBar(theSnackBar);
  }
}

class FamigliaTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          //set the color of the card
          color: Colors.redAccent,
          child: Column(
              children: [
                ListTile(
                  //leading is the picture
                  leading: famigliaPicture,
                  //title is the name given in the form
                  title: famigliaName,
                  //subtitle is the info provided by the form
                  subtitle: famigliaInfo,
                ),
                //Button bar to hold the buttons
                ButtonBar(
                  alignment: MainAxisAlignment.end,
                  children: [
                    //button to send emails, includes tooltip
                    IconButton(
                      icon: Icon(Icons.attach_money, color: Colors.white60),
                      tooltip: "Donate Money",
                      onPressed: (){
                        showSnackBar(context);
                      },
                    ),
                    //button to share info, includes tooltip
                    IconButton(
                      icon: Icon(Icons.airline_seat_flat, color: Colors.white60),
                      tooltip: "Hire a Hit",
                      onPressed: (){
                        showSnackBar(context);
                      },
                    ),
                  ],
                )
              ]
          )
      ),
    );
  }
  void showSnackBar(BuildContext context) {
    //the function to show the snackBar
    SnackBar theSnackBar = new SnackBar(
      //if choice is true, snack bar == accepted, if choice is false, snackbar == decline
      content: Text("It still doesn't actually do anything, silly"),
      action: SnackBarAction(
        label: 'Get rid of it then!',
        onPressed: () {
        },
      ),
      duration: const Duration(seconds: 3),
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
    );
    //Snackbar always paired to the scaffold
    Scaffold.of(context).showSnackBar(theSnackBar);
  }
}

//Container that holds the profile picture
final profilePicture = new Container(
  child: Image.asset("images/profileicon.png",
      fit: BoxFit.cover),

);

//Container that holds the famiglia picture
final famigliaPicture = new Container(
  child: Image.asset("images/mafia.jpg",
      fit: BoxFit.cover),

);

//Container that holds the profile name
final profileName = new Container(
  child: Text(name + lname, style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold, color: Colors.black, fontStyle: FontStyle.italic))
);

//Container that holds the famiglia name
final famigliaName = new Container(
    child: Text(famiglia, style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold, color: Colors.black, fontStyle: FontStyle.italic))
);

//Container that holds the profile information
final profileInfo = new Container (
  height: 150,
  width: 300,
  child: ListView(
    children: [
      RichText(
          text: TextSpan(
              style: TextStyle(fontSize: 15.0, color: Colors.black,),
              children: <TextSpan> [
                TextSpan(text: "\n", style: TextStyle(fontSize: 6.0)),
                TextSpan(text: '  Name: ', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: name + lname),
              ]
          )),
      RichText(
          text: TextSpan(
              style: TextStyle(fontSize: 15.0, color: Colors.black,),
              children: <TextSpan> [
                TextSpan(text: "\n", style: TextStyle(fontSize: 6.0)),
                TextSpan(text: '  Age: ', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: age),
              ]
          )),
      RichText(
          text: TextSpan(
              style: TextStyle(fontSize: 15.0, color: Colors.black,),
              children: <TextSpan> [
                TextSpan(text: "\n", style: TextStyle(fontSize: 6.0)),
                TextSpan(text: '  Gender: ', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: gender),
              ]
          )),
      RichText(
          text: TextSpan(
              style: TextStyle(fontSize: 15.0, color: Colors.black,),
              children: <TextSpan> [
                TextSpan(text: "\n", style: TextStyle(fontSize: 6.0)),
                TextSpan(text: '  Famiglia: ', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: famiglia),
              ]
          )),
    ]
  )
);

final famigliaInfo = new Container (
    height: 150,
    width: 300,
    child: ListView(
        children: [
          RichText(
              text: TextSpan(
                  style: TextStyle(fontSize: 15.0, color: Colors.black,),
                  children: <TextSpan> [
                    TextSpan(text: "\n", style: TextStyle(fontSize: 6.0)),
                    TextSpan(text: '  Established: ', style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: '2020'),
                  ]
              )),
          RichText(
              text: TextSpan(
                  style: TextStyle(fontSize: 15.0, color: Colors.black,),
                  children: <TextSpan> [
                    TextSpan(text: "\n", style: TextStyle(fontSize: 6.0)),
                    TextSpan(text: '  Members: ', style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: '1'),
                  ]
              )),
          RichText(
              text: TextSpan(
                  style: TextStyle(fontSize: 15.0, color: Colors.black,),
                  children: <TextSpan> [
                    TextSpan(text: "\n", style: TextStyle(fontSize: 6.0)),
                    TextSpan(text: '  Your Rank: ', style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: 'Rookie'),
                  ]
              )),
          RichText(
              text: TextSpan(
                  style: TextStyle(fontSize: 15.0, color: Colors.black,),
                  children: <TextSpan> [
                    TextSpan(text: "\n", style: TextStyle(fontSize: 6.0)),
                    TextSpan(text: '  Mission: ', style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: 'Run The World'),
                  ]
              )),
        ]
    )
);